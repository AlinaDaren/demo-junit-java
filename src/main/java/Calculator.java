public class Calculator {

    private int _a;
    private int _b;

    public Calculator() {
    }

    public Calculator(int a, int b) {
        _a = a;
        _b = b;
    }

    public double sum(double a, double b) {
        return a + b;
    }

    public double subtract(double a, double b) {
        return a - b;
    }

    public long multiply(long a, long b) {
        return a * b;
    }

    public float divide(float a, float b) {
        return a / b;
    }

    public int sum() {
        return _a + _b;
    }

    public int sub() {
        return _a - _b;
    }

    public int multiply() {
        return _a * _b;
    }


}
