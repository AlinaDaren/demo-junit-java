import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testSum() {
        Calculator calculator = new Calculator();
        double actual = calculator.sum(2.1, 2.1);

        Assert.assertEquals(4.2, actual, 0.01);
    }

    @Test
    public void testSubtraction() {
        Calculator calculator = new Calculator();
        double actual = calculator.subtract(5.1, 4.0);

        Assert.assertEquals(1.1, actual, 0.01);
    }

    @Test
    public void testMulti() {
        Calculator calculator = new Calculator();
        long actual = calculator.multiply(50, 50);

        Assert.assertEquals(2500, actual);
    }

    @Test
    public void testDiv() {
        Calculator calculator = new Calculator();
        float actual = calculator.divide(100.0f, 20.0f);

        Assert.assertEquals(5f, actual, 0.01);
    }

    @Test
    public void testSum2() {
        Calculator calculator = new Calculator(11, 6);
        Assert.assertEquals(17, calculator.sum());
    }

    @Test
    public void testSub() {
        Calculator calculator = new Calculator(15, 5);
        Assert.assertEquals(10, calculator.sub());
    }

    @Test
    public void testMultiply() {
        Calculator calculator = new Calculator(5, 5);
        //int actual = calculator.multiply();
        Assert.assertEquals(25, calculator.multiply());
    }

}
